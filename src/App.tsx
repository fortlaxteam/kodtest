import React from 'react';
import './App.css';
import Component1 from './component1';

function App(): JSX.Element {
	return <>
		<div style={{ border: "1px solid black", margin: "10px", padding: "10px" }}>
			<h1>Development task</h1>
			<p>1. Examine the code and give opinions on how to refactor.</p>
			<p>2. Add Typescript to GET call.</p>
			<p>3. Fetch list of todos and render them in a table style.</p>
			<p><b>Good luck</b></p>
		</div>
		<Component1 ID="Foo" />
	</>;

}

export default App;
