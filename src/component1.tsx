import React from 'react';
import { GET } from './makehttprequest';
interface Props {
	ID?: string
}

const Index = (props: Props): JSX.Element => {

	React.useEffect(() => {
		GET("/todos/1").then(r => {
			console.log(r);
		});
	}, []);


	return <h1>Hello, world. Props={JSON.stringify(props)}</h1>;
};

export default Index;