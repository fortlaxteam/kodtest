import axios, { AxiosRequestConfig, Method } from "axios";

const BASEURL = "https://jsonplaceholder.typicode.com";

// eslint-disable-next-line require-await
export async function GET<T>(iRelativeURL: string): Promise<{ success: true, body: T } | { success: false, error: string }> {
	return Req("GET", iRelativeURL);
}

// eslint-disable-next-line require-await
export async function POST<T>(iRelativeURL: string, iPayload?: unknown): Promise<{ success: true, body: T } | { success: false, error: string }> {
	return Req("GET", iRelativeURL, iPayload);
}


async function Req<T>(iMethod: Method, iRelativeURL: string, iPayload?: unknown): Promise<{ success: true, body: T } | { success: false, error: string }> {

	if (!iRelativeURL.startsWith("/"))
		throw Error("Must start with /");

	console.log(`${iMethod} ${iRelativeURL}`);
	if (iPayload)

		console.log(iPayload);

	const axiosConfig: AxiosRequestConfig = {
		method: iMethod,
		url: `${BASEURL}${iRelativeURL}`,
		headers: {
			"Accept": "application/json",
		},
		data: iPayload
	};

	try {
		console.log(axiosConfig);
		const response = await axios(axiosConfig);
		return { success: true, body: await response.data };
	} catch (e) {
		console.log(e);
		return { success: false, error: (e as Error).message };
	}
}
