# Setup

## vscode

### React dev env

With typescript

```bash
npx create-react-app app1 --template typescript
cd app1
```

### Eslint

```bash
npx eslint --init

# After install, verify install

node_modules\.bin\eslint **/*.ts

# Next, copy .eslintrc and verify again

node_modules\.bin\eslint **/*.ts
```

Optionally add to package.json

```json
 "eslint": "eslint src/**/**.ts",
```

## Settings.json

Copy settings.json and verify that the settings are working in vscode.

## Return type

App.tsx - add `: JSX.Element`

## Start react

```bash
set DISABLE_ESLINT_PLUGIN=true
npm start
```

## API Documentation

Site: <https://jsonplaceholder.typicode.com/>

### Fetch todo

```bash
curl https://jsonplaceholder.typicode.com/todos/1
```

Response

```json
{
  "userId": 1,
  "id": 1,
  "title": "delectus aut autem",
  "completed": false
}
```

### More examples

<https://jsonplaceholder.typicode.com/guide/>
